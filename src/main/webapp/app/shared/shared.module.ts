import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhgreenmailexampleSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhgreenmailexampleSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [JhgreenmailexampleSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhgreenmailexampleSharedModule {
  static forRoot() {
    return {
      ngModule: JhgreenmailexampleSharedModule
    };
  }
}
